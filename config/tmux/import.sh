mkdir -p $HOME/.config/tmux/

git clone https://github.com/tmux-plugins/tpm $HOME/.config/tmux/plugins/tpm

TMUX_VERSION=$(tmux -V)
MAJOR_VERSION=$(echo "$TMUX_VERSION" | awk '{print $2}' | cut -d'.' -f1)
MINOR_VERSION=$(echo "$TMUX_VERSION" | awk '{print $2}' | cut -d'.' -f2 | cut -d'a' -f1)

if [ -z "$MAJOR_VERSION" ] || [ -z "$MINOR_VERSION" ]; then
  echo "Unable to parse tmux version."
elif [ "$MAJOR_VERSION" -ge 3 ] && [ "$MINOR_VERSION" -ge 0 ]; then
  echo "tmux version is 3.0 or higher."
  DEST="$HOME/.config/tmux/.tmux.conf"
else
  echo "tmux version is older than 3.0."
  DEST="$HOME/.tmux.conf"
fi

ln -sf $PWD/tmux.conf $DEST
